/**
 * @file
 * Behaviors for the uptown theme.
 */

(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.uptown = {
    attach: function (context) {
  //     	$('.slider').slick({
		// 	autoplay: true,
		// 	arrows: true,
		// 	pauseOnHover: true,
		// 	autoplay: true,
  //     		autoplaySpeed: 8000,
  //     		mobileFirst: true,
		// });

		// var $slider = $('.your-class').slick({
		// 	autoplay: true
		// });
		// $slider.slick('getSlick').reinit();

		// Floor map
		$('#mapplic').mapplic({
			source: drupalSettings.floor_plan,
			height: 480,
			minimap: false,
			developer: false,
			search: false,
			mousewheel: false,
			// mapfill: true,
			lightbox: true,
			maxscale: 1
		});
      }
  };

})(window.jQuery, window._, window.Drupal, window.drupalSettings);
