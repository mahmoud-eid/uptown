<?php

namespace Drupal\floor_plan\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\image\Entity\ImageStyle;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\JsonResponse;

class LocationsImagesController extends ControllerBase {

  /**
   * .
   *
   * @return array
   */
  public function upload(Request $request) {
  	$file = $request->files->get('image');
  	$savedFile = file_save_data(file_get_contents($file), $destination = NULL, $replace = FILE_EXISTS_RENAME);

  	$result = [
  		"file_id" => $savedFile->id(),
  		"image_uri" => $savedFile->getFileUri(),
  		"image_original" => file_create_url($savedFile->getFileUri()),
  		"image_thumbnail" => ImageStyle::load('thumbnail')->buildUrl($savedFile->getFileUri())
  	];

    return new JsonResponse($result);
  }

}