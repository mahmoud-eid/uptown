/**
 * @file
 * Behaviors for the floor plan module.
 */

(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.uptown = {

    attach: function (context) {
      var floor_plans = {};
      //  $(".image-preview img").click(function(e) {
      //  var offset = $(this).offset();
      //  var relativeX = (e.pageX - offset.left);
      //  var relativeY = (e.pageY - offset.top);
      //  alert('Copy the following x:'+relativeX+' and y:'+relativeY+' to your location\'s x and y fields.');
      // });
      $('.field--name-field-floor-plan-data').css('display', 'none');

        /*
         * Init.
         */
        function init() {
          listenToFloorPlanClick();

          // Check if there are old plans
          var oldFloorPlan = $('#edit-field-floor-plan-data-0-value').text();
          floor_plans = $.parseJSON(oldFloorPlan);
          initFloorPlans();

          //Listen to remove one of floor plans
          $('.field--name-field-floor-plans table tbody tr td:last-of-type').once().bind('DOMNodeRemoved', function(element) {
            deleteRemovedFloorPlan('floor_plan_'+ $(this).parent().find('.image-widget-data input[type="hidden"]:first').val());
          });
        }
        init();

        /*
         * Delete removed floor plan from object.
         */
        function deleteRemovedFloorPlan(floorPlanId) {
          // loop through floor plans in page
          console.log(floorPlanId);
          if(floorPlanId != ''){
            delete floor_plans[floorPlanId];
            // Save all
            saveFloorPlansData();
          }
        }

        /*
         * Listen to floor plan click.
         */
        function listenToFloorPlanClick() {
          $('.field--name-field-floor-plans img').off('click').on('click', function (event) {
            var position = {
              x: event.originalEvent.offsetX,
              y: event.originalEvent.offsetY
            }
            addMarker(this, position);
          });
        }

        /*
         * Init floor plans and add locations to each plan.
         */
        function initFloorPlans() {
          // Loop through all floor plans
          $.each(floor_plans, function( floor_plan_key, floor_plan_data ) {
            // then loop though each floor plan markers
            $.each(floor_plan_data, function( marker_key, marker_data ) {
              // then loop though each floor plan markers
              // Check if marker not added before or not in html
              if($('#'+marker_key).length == 0) {
                var position = {
                  x: marker_data.floor_plan_location_x,
                  y: marker_data.floor_plan_location_y
                }
                // Get fid from key
                var fileId = floor_plan_key.replace('floor_plan_','');
                var $image = $('.image-widget-data input[type="hidden"][value="'+fileId+'"]:first').parent().parent().find('.image-preview img:first');
                addMarker($image, position, true, marker_data);
              }
            });
          });
        }        

        /*
         * Add marker to image.
         */
        function addMarker(element, position, editMode = false, marker_data = {}) {
          var floorPlanId = 'floor_plan_' + $(element).parent().parent().find('.image-widget-data input[type="hidden"]:first').val();
          var markersCount = $('[data-floor-plan-id="'+ floorPlanId +'"]').length;
          var markerNumber = markersCount + 1;
          var markerId = floorPlanId+'_marker_'+markerNumber;
          var offset = $(element).parent().offset();
          $(element).after(        
              $('<div id="'+ markerId +'" class="marker" data-floor-plan-id="'+ floorPlanId +'"></div>').css({
                position: 'absolute',
                top: position.y + 'px',
                left: position.x+ 'px',
                width: '15px',
                height: '15px',
                background: 'red',
                'border-radius': '50px'
              })              
          );
          // Save marker X and Y
          saveMarkerPosition(floorPlanId, markerId, position.x, position.y);
          // Add marker data form
          addMarkerDataForm(element, floorPlanId, markerId, editMode, marker_data);
          // Listen to edit marker data form event
          editMarkerDataListener();
          // Listen to move marker event
          moveMarkerListener();
        }

        /*
         * Add marker data form.
         */
        function addMarkerDataForm(element, floorPlanId, markerId, editMode, marker_data) {
          var form = '<div class="marker-form" data-marker-id="'+ markerId +'">';
          //form += '<div class="close-marker-button"><span>x</span></div>';
          form += '<div class="form-item inline-form-item"><label class="form-required" for="'+ markerId +'_floor_plan_location_name">Name</label><input type="text" id="'+ markerId +'_floor_plan_location_name" class="form-text required-field" name="floor_plan_location_name" value="" size="28"></div>';
          form += '<div class="form-item inline-form-item"><label class="form-required" for="'+ markerId +'_floor_plan_location_category">Category</label><input type="text" id="'+ markerId +'_floor_plan_location_category" class="form-text required-field" name="floor_plan_location_category" value="" size="28"></div>';
          form += '<div class="form-item"><label for="'+ markerId +'_floor_plan_location_about">About</label><input type="text" id="'+ markerId +'_floor_plan_location_about" class="form-text" name="'+ markerId +'_floor_plan_location_about" value="" size="60"></div>';
          
          // Pin
          form += '<div class="form-item inline-form-item"><label for="floor_plan_location_pin">Pin</label>';
          form +='<select name="floor_plan_location_pin" id="'+ markerId +'_floor_plan_location_pin" class="form-select">';
          form +='<option value="red" selected>Red</option>';
          form +='<option value="circular">Circular</option>';
          
          form += '</select>';
          form += '</div>';

          // Action
          form += '<div class="form-item inline-form-item"><label for="'+ markerId +'_floor_plan_location_action">Action</label>';
          form +='<select name="floor_plan_location_action" id="'+ markerId +'_floor_plan_location_action" class="form-select">';
          form +='<option value="lightbox" selected>Lightbox</option>';
          form +='<option value="tooltip">tooltip</option>';
          form += '</select>';
          form += '</div>';

          // Zoom
          form += '<div class="form-item inline-form-item"><label for="'+ markerId +'_floor_plan_location_zoom">Zoom</label>';
          form +='<select name="floor_plan_location_zoom" id="'+ markerId +'_floor_plan_location_zoom" class="form-select">';
          form +='<option value="1">1</option>';
          form +='<option value="2">2</option>';
          form +='<option value="3" selected>3</option>';
          form +='<option value="4">4</option>';

          form += '</select>';
          form += '</div>';
          
          // Start image form
          var imageForm = '<form class="location_image" method="post" enctype="multipart/form-data">'
          imageForm += '<div class="form-item">';
          imageForm += '<label>Image</label>';
          imageForm += '<input type="file" name="image" />';
          imageForm += '</div>';
          imageForm += '<div class="description">32 MB limit.<br>Allowed types: png gif jpg jpeg.</div>';
          imageForm += '</form>';
          form += imageForm;
          // End image form

          // Actions
          form += '<div class="form-wrapper">';
          form += '<div>';
          form += '<a href="javascript:void(0)" class="save-button button form-submit">Save</a>';
          form += '<a href="javascript:void(0)" class="delete-button button form-submit">Delete</a>';
          form += '</div>';

          form += '</div>';
          $(element).after(form);
          // Check if edit mode is enabled hide form
          if(editMode == true) {
            $('[data-marker-id="'+ markerId +'"]').hide();
          }
          // Check if marker data not empety in edit mode
          if($.isEmptyObject(marker_data) != true) {
            addMarkerDataFormInEditMode(markerId, marker_data);
          }
          // Listen to close marker event
          //closeMrakerFormListener();
          // Listen to upload image
          uploadImageListener();
          // Listen to operations button
          operationsListener(floorPlanId, markerId);
        }

        /*
         * Fill markers forms in edit mode.
         */
        function addMarkerDataFormInEditMode(markerId, marker_data) {
          $('#'+ markerId +'_floor_plan_location_name').val(marker_data.floor_plan_location_name);
          $('#'+ markerId +'_floor_plan_location_category').val(marker_data.floor_plan_location_category);
          $('#'+ markerId +'_floor_plan_location_about').val(marker_data.floor_plan_location_about);
          $('#'+ markerId +'_floor_plan_location_pin').val(marker_data.floor_plan_location_pin);
          $('#'+ markerId +'_floor_plan_location_action').val(marker_data.floor_plan_location_action);
          $('#'+ markerId +'_floor_plan_location_zoom').val(marker_data.floor_plan_location_zoom);
          // Image data
          $('[data-marker-id="'+ markerId +'"] .location_image .form-item').children('.thumbnail').remove();
          var imageComponent = '<div class="thumbnail">';

          if(marker_data.floor_plan_location_image_thumbnail_url) {
            imageComponent += '<img src="'+marker_data.floor_plan_location_image_thumbnail_url+'" />';
          }

          if(marker_data.floor_plan_location_image_file_id) {
            imageComponent += '<input type="hidden" class="file_id" value="'+marker_data.floor_plan_location_image_file_id+'">';
          }

          if(marker_data.floor_plan_location_image_uri) {
            imageComponent += '<input type="hidden" class="image_uri" value="'+marker_data.floor_plan_location_image_uri+'">';
          }
          
          if(marker_data.floor_plan_location_image_thumbnail_url) {
            imageComponent += '<input type="hidden" class="image_thumbnail_url" value="'+marker_data.floor_plan_location_image_thumbnail_url+'">';
          }

          if(marker_data.floor_plan_location_image_original_url) {
            imageComponent += '<input type="hidden" class="image_original_url" value="'+marker_data.floor_plan_location_image_original_url+'">';
          }
          
          imageComponent += '</div>';
          $('[data-marker-id="'+ markerId +'"] .location_image .form-item input').after(imageComponent);
        }

        /*
         * Move marker.
         */
        function moveMarkerListener() {
          $('.marker').on('mousedown', function (e) {
              $(this).addClass('active').parents().on('mousemove', function (e) {
                  $('.active').offset({
                      top: e.pageY - $('.active').outerHeight() / 2,
                      left: e.pageX - $('.active').outerWidth() / 2
                  }).on('mouseup', function () {
                      $(this).removeClass('active');
                  });
              });
              return false;    
          });
        }

        /*
         * Edit marker form data listener
         */
        function editMarkerDataListener() {
          $('.marker').dblclick(function(){
                $('[data-marker-id="'+this.id+'"]').show();
            });
        }

        /*
         * Close marker form data listener
         */
        function closeMrakerFormListener() {
          $('.close-marker-button').on('click', function (event) {
            $(this).parent().hide();
          });
        }

        /*
         * Upload location image.
         */
        function uploadImageListener() {
          $('.location_image input[type="file"]').on('change', function () {
            var $currentLocationImage = $(this);
            console.log('start upload');
            // Start upload 
            $.ajax({
              url: drupalSettings.path.baseUrl + 'ajax/location-image-upload',
              type: 'POST',
              dataType: 'json',
              contentType:false,
              processData:false,
              data: new FormData($(this).parent().parent()[0]),
              success:function(response) {
                $currentLocationImage.parent().children('.thumbnail').remove();
                var imageComponent = '<div class="thumbnail">';
                imageComponent += '<img src="'+response.image_thumbnail+'" />';
                imageComponent += '<input type="hidden" class="file_id" value="'+response.file_id+'">';
                imageComponent += '<input type="hidden" class="image_uri" value="'+response.image_uri+'">';
                imageComponent += '<input type="hidden" class="image_thumbnail_url" value="'+response.image_thumbnail+'">';
                imageComponent += '<input type="hidden" class="image_original_url" value="'+response.image_original+'">';
                imageComponent += '</div>';

                $currentLocationImage.after(imageComponent);
              }
            });

          });
        }

        /*
         * Operations save and delete.
         */
        function operationsListener(floorPlanId, markerId) {
          // Save listener.
          $('.marker-form[data-marker-id="'+ markerId +'"] .save-button').on('click', function(event) {
            // Save data
            if(checkRequiredFields(markerId)) {
              saveMrakerData(floorPlanId, markerId);
              // Close form
              $('.marker-form[data-marker-id="'+ markerId +'"]').hide();
            }
          });

          // Delete listener.
          $('.marker-form[data-marker-id="'+ markerId +'"] .delete-button').on('click', function(event) {
            // Close form
            $('.marker-form').hide();
            // Delete data from object
            deleteMarkerData(floorPlanId, markerId);
            // Delete form
            $('.marker-form[data-marker-id="'+markerId+'"]').remove();
            // Delete marker
            $('#'+markerId).remove();
          });
        }

        /*
         * Check required fields.
         */
        function checkRequiredFields(markerId) {
          var required = 0;
          $('.marker-form[data-marker-id="'+ markerId +'"] .required-field').each(function(index, el) {
            $(this).css('border-color', 'unset');
            if($(this).val().length == 0) {
              $(this).css('border-color', 'red');
              required++;
            }
          });

          if(required == 0){
            return true;
          }
          return false;
        }

        /*
         * Save X and Y of marker
         */
        function saveMarkerPosition(floorPlanId, markerId, x, y) {
          // init plan id
          if(!floor_plans[floorPlanId]) {
            floor_plans[floorPlanId] = {};
          }
          // init marker id
          if(!floor_plans[floorPlanId][markerId]){
            floor_plans[floorPlanId][markerId] = {};
          }

          floor_plans[floorPlanId][markerId]['floor_plan_location_x'] = x;
          floor_plans[floorPlanId][markerId]['floor_plan_location_y'] = y;
          // Save all
          saveFloorPlansData();
        }

        /*
         * 
         */
        function saveMrakerData(floorPlanId, markerId) {
          // init plan id
          if(!floor_plans[floorPlanId]) {
            floor_plans[floorPlanId] = {};
          }
          // init marker id
          if(!floor_plans[floorPlanId][markerId]){
            floor_plans[floorPlanId][markerId] = {};
          }
          
          floor_plans[floorPlanId][markerId]['floor_plan_location_name'] = validateData($('#'+markerId+'_floor_plan_location_name').val());
          floor_plans[floorPlanId][markerId]['floor_plan_location_category'] = validateData($('#'+markerId+'_floor_plan_location_category').val());
          floor_plans[floorPlanId][markerId]['floor_plan_location_about'] = validateData($('#'+markerId+'_floor_plan_location_category').val());
          floor_plans[floorPlanId][markerId]['floor_plan_location_pin'] = validateData($('#'+markerId+'_floor_plan_location_pin').val());
          floor_plans[floorPlanId][markerId]['floor_plan_location_action'] = validateData($('#'+markerId+'_floor_plan_location_action').val());
          floor_plans[floorPlanId][markerId]['floor_plan_location_zoom'] = validateData($('#'+markerId+'_floor_plan_location_zoom').val());
          floor_plans[floorPlanId][markerId]['floor_plan_location_image_file_id'] = $('[data-marker-id="'+ markerId +'"] .file_id').val();
          floor_plans[floorPlanId][markerId]['floor_plan_location_image_uri'] = $('[data-marker-id="'+ markerId +'"] .image_uri').val();
          floor_plans[floorPlanId][markerId]['floor_plan_location_image_thumbnail_url'] = $('[data-marker-id="'+ markerId +'"] .image_thumbnail_url').val();
          floor_plans[floorPlanId][markerId]['floor_plan_location_image_original_url'] = $('[data-marker-id="'+ markerId +'"] .image_original_url').val();

          // Save all
          saveFloorPlansData();
        }

        /*
         * 
         */
        function validateData(data) {
          if(data != ''){
            return data;
          }
          return '';
        }

        /*
         * Delete marker data from object.
         */
        function deleteMarkerData(floorPlanId, markerId) {
          delete floor_plans[floorPlanId][markerId];
          // Save all
          saveFloorPlansData();
        }

        /*
         * Save all data to hidden field.
         */
        function saveFloorPlansData() {
          // var data = JSON.stringify(floor_plans,undefined, 2);
          console.log(floor_plans);
          $('#edit-field-floor-plan-data-0-value').text(JSON.stringify(floor_plans));
        }
      }
  };

})(window.jQuery, window._, window.Drupal, window.drupalSettings);
